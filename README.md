openssl-aes
===========

AES crypto library from openssl

this is aes crypto algorithm implemention ported from openssl-1.0.1e, i removed all un-related files to make the library lightweight.

it has 2 parts:
(I)aes algorithm from openssl library
	A)changes:
		1)Nothing of the codes be changed excepted:'Makefile';
		2)Directory organization of some files changed;
		3)header files including changed.
		4)add android-support, you can compile it as an android .so library

	B)how to use:
		1) complingq the library, quite simple!
		[ra@scrcsl-server11 aes]$ cd openaes;make clean;make
		cc -I ./include -g   -c -o src/aes_misc.o src/aes_misc.c
		cc -I ./include -g   -c -o src/aes_ecb.o src/aes_ecb.c
		cc -I ./include -g   -c -o src/aes_cfb.o src/aes_cfb.c
		cc -I ./include -g   -c -o src/aes_ofb.o src/aes_ofb.c
		cc -I ./include -g   -c -o src/aes_ctr.o src/aes_ctr.c
		cc -I ./include -g   -c -o src/aes_ige.o src/aes_ige.c
		cc -I ./include -g   -c -o src/aes_wrap.o src/aes_wrap.c
		cc -I ./include -g   -c -o src/aes_core.o src/aes_core.c
		cc -I ./include -g   -c -o src/aes_cbc.o src/aes_cbc.c
		cc -I ./include -g   -c -o src/cbc128.o src/cbc128.c
		cc -I ./include -g   -c -o src/ctr128.o src/ctr128.c
		cc -I ./include -g   -c -o src/cts128.o src/cts128.c
		cc -I ./include -g   -c -o src/cfb128.o src/cfb128.c
		cc -I ./include -g   -c -o src/ofb128.o src/ofb128.c
		cc -I ./include -g   -c -o src/gcm128.o src/gcm128.c
		cc -I ./include -g   -c -o src/ccm128.o src/ccm128.c
		cc -I ./include -g   -c -o src/xts128.o src/xts128.c
		ar r libcrypto.a src/aes_misc.o src/aes_ecb.o src/aes_cfb.o src/aes_ofb.o src/aes_ctr.o src/aes_ige.o src/aes_wrap.o src/aes_core.o src/aes_cbc.o src/cbc128.o src/ctr128.o src/cts128.o src/cfb128.o src/ofb128.o src/gcm128.o src/ccm128.o src/xts128.o 
		ar: creating libaes.a

		2) add 'include' directory to your project
		3) make you program link to 'libaes.a'.It will work!
			you can reference with the samples below.

II)wrapper interface:a wrapper class and sample, locate in "app" direcotry
	A)how to use:
	[ra@scrcsl-server11 aes]$ cd app;make clean;make
	[ra@scrcsl-server11 aes]$ ./xencryptor  ../test_data_2/plain ../test_data_2/encrypted ../test_data_2/decrypted

	B)for GuessStock project, just run bash script : compile_and_install_android_jni.sh





*************************************************
license:
*************************************************
kepp openssl original license unchanged.

 

 

Zhengfeng Rao
2013-3-20
