#include <jni.h>
#include <android/log.h>
#include "com_bochengyishi_guessstock_GameData.h"
#include "aeswrapper.h"

JNIEXPORT jstring JNICALL Java_com_bochengyishi_guessstock_GameData_DecodeBuffer
  (JNIEnv * env, jclass, jbyteArray buff, jint length)
{
	int len = length;
	char* encrypted_buff = (char*)env->GetByteArrayElements(buff, 0);
        char* decrypted_buff = new char[len*2];
        memset(decrypted_buff, 0, len*2);
        AesWarpper decrypter;
        decrypter.Decrypt((unsigned char*)encrypted_buff, (unsigned char*)decrypted_buff, len);

	/*
	char pLen[10]={0};
	sprintf(pLen, "%d", len);
	LOG(pLen);
	*/

	jstring ret = env->NewStringUTF(decrypted_buff);
	delete decrypted_buff;

	return ret;
}

