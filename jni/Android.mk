LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

OPENAES_SRC = ../openaes/src
LOCAL_MODULE    := haha
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := \
	$(OPENAES_SRC)/cryptlib.c \
 	$(OPENAES_SRC)/aes_misc.c \
	$(OPENAES_SRC)/aes_ecb.c \
	$(OPENAES_SRC)/aes_cfb.c \
	$(OPENAES_SRC)/aes_ofb.c \
	$(OPENAES_SRC)/aes_ctr.c \
	$(OPENAES_SRC)/aes_ige.c \
	$(OPENAES_SRC)/aes_wrap.c \
	$(OPENAES_SRC)/aes_core.c \
	$(OPENAES_SRC)/aes_cbc.c \
	$(OPENAES_SRC)/cbc128.c \
	$(OPENAES_SRC)/ctr128.c \
	$(OPENAES_SRC)/cts128.c \
	$(OPENAES_SRC)/cfb128.c \
	$(OPENAES_SRC)/ofb128.c \
	$(OPENAES_SRC)/gcm128.c \
	$(OPENAES_SRC)/ccm128.c \
	$(OPENAES_SRC)/xts128.c \
	GameData_Decode.cpp

LOCAL_C_INCLUDES :=$(JNI_H_INCLUDE) app/ openaes/include/
LOCAL_LDLIBS += -L$(SYSROOT)/usr/lib -llog 
LOCAL_C_FLAGS := -DAndroid 

#*********************** NOTICE ************************
#we should enable this option
#
#some functions(sk_new_null(), BUF_strdup(), ERR_put_error()) are undefined in this project
#they are called in functions suach as CRYPTO_get_new_lockid() in file src/cryptolib.c
#but CRYPTO_get_new_lockid() will never be called in fact
#to keep less modification in this projectm i keep the code in file src/cryptolib.c unremoved.
#
#in normal linux systems, it will work well
#but in Android NDK, the compiler will complain 'undefined reference',
#that's because Android NDK treate this as an error to help developers discover potential bugs.
#so we need to disable this feature in NDK.
#see file:android-ndk-r9b/documentation.html
#	By default, any undefined reference encountered when trying to build a shared library will 
#	result in an "undefined symbol" error. This is a great help to catch bugs in your source code.
#	However, if for some reason you need to disable this check, set this variable to 'true'. 
#	Note that the corresponding shared library may fail to load at runtime.
#	NOTE: This is ignored for static libraries, and ndk-build will print a warning if you define it in such a module.
#
# ***** this option won't work *****
#this option will make NDK compiler won't complain on undefined reference, but Android APK will throw exception
#for these undefined references.
#so, we still need to remove the unused undefined functions
#LOCAL_ALLOW_UNDEFINED_SYMBOLS := true

LOCAL_SHARED_LIBRARIES := liblog libcutils
LOCAL_PRELINK_MODULE := false
include $(BUILD_SHARED_LIBRARY)
