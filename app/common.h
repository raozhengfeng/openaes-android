/*
 * File: common.h
 * Author: chu
 *
 * Created on 2013年3月9日, 下午6:36
 */

#ifndef COMMON_H
#define COMMON_H

#include <stdlib.h>
#include <string.h>
#include <iostream>

//aes crypt
typedef unsigned int u32;
typedef unsigned char u8;
#define GETU32(pt) (((u32)(pt)[0] << 24) ^ ((u32)(pt)[1] << 16) ^ ((u32)(pt)[2] << 8) ^ ((u32)(pt)[3]))
#define PUTU32(ct, st) { (ct)[0] = (u8)((st) >> 24); (ct)[1] = (u8)((st) >> 16); (ct)[2] = (u8)((st) >> 8); (ct)[3] = (u8)(st); }
#define MAX_BUFFER_LEN 1024*1024
#define AES_MAX_BUFFER_LEN MAX_BUFFER_LEN-8

#ifdef ANDROID
	#include <android/log.h>
	#define LOG(msg) \
	 __android_log_print(ANDROID_LOG_DEBUG, "JNI", "%s", msg);
#else
	#define LOG(msg) \
	std::cout<<msg<<std::endl;
#endif

#endif /* COMMON_H */
