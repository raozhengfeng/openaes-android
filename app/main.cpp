#include "encrypter.h"

const char* g_szPlainDir = NULL;
const char* g_szEncryptedDir = NULL;
const char* g_szDecryptedDir = NULL;

void (*g_pEncryptor)(const char*, const char*) ;
void (*g_pDecryptor)(const char*, const char*) ;

void show_help()
{
	printf("usage:\e[1mxencryptor\e[0m \e[4mPLAIN_TEXT_DIRECTORY\e[0m \e[4mENCRYPTED_DIRECTORY\e[0m \e[4mDECRYPTED_DIRECTORY\e[0m\n");
}

bool process_params(int argc, char** argv)
{
	do
	{
		if(argc != 4)
		{	
			break;
		}

		g_szPlainDir = argv[1];
		g_szEncryptedDir = argv[2];
		g_szDecryptedDir = argv[3];

		if((g_szPlainDir == NULL) || (g_szEncryptedDir == NULL) || (g_szDecryptedDir == NULL))
		{
			break;
		}
		return true;
	}while(false);

	printf("Invalid params.\n");
	show_help();

	return false;
}

void select_encryptor()
{
	int choose = -1;

	do
	{
		printf("Please select encrypt/decrypt method:\n");
		printf("\t[1] encrypt/decrypt whole file.\n");
		printf("\t[2] encrypt/decrypt file in block(1k per block).\n");
		scanf("%d", &choose);
		if(choose == 1)
		{
			g_pEncryptor = encrypt_whole_file;
			g_pDecryptor = decrypt_whole_file;
		}
		else if(choose == 2)
		{
                        g_pEncryptor = encrypt_perK_file;
                        g_pDecryptor = decrypt_perK_file;

		}
		else
		{
			printf("Invalid choose, please try again.\n");
		}
	}while(choose == -1);
}

int main(int argc, char** argv)
{
	if(process_params(argc, argv) == false)
	{
		exit(EXIT_FAILURE);
	}

	select_encryptor();

	scan_dir(g_szPlainDir, g_szEncryptedDir, g_pEncryptor);
	scan_dir(g_szEncryptedDir, g_szDecryptedDir, g_pDecryptor);

	return 0;
}

