#include "aeswrapper.h"

void scan_dir(const char* dir_path, const char* out_path, void (*fun)(const char*, const char*));

void encrypt_whole_file(const char* file_name, const char* out_file);
void decrypt_whole_file(const char* file_name, const char* out_file);

void encrypt_perK_file(const char* file_name, const char* out_file);
void decrypt_perK_file(const char* file_name, const char* out_file);

