/*
 * File: AesWarpper.h
 * Author: raozf
 *
 * Created on 2013年3月21日, 下午2:37
 */

#ifndef AESWARPPER_H
#define AESWARPPER_H

#include "stdio.h"
#include <string>

#include "aes.h"
#include "common.h"

class AesWarpper
{
private:
    //CBC:pad with 0
    //ECB:undefined, usually PCKS5Padding

    void PadData(const char* str, int& len, unsigned char** out, int blocksize)
    {
        unsigned char _pad = (unsigned char) (blocksize - len % blocksize);
        len += _pad;
        *out = (unsigned char*) malloc(len * sizeof (char));
        memset(*out, 0, len);
        memcpy(*out, str, len * sizeof (char));
    }

    int RangeRandom(int min, int max)
    {
        if (max > min)
            return random() % (max - min + 1) + min;
        else
            return random();
    }

    void GenerateKey(int key_index, unsigned char* key, unsigned int key_size)
    {
        for (int i = 0; i < key_size; i++)
        {
            //TODO: need more mix operations
            key[i] = (i + 435)*20^43657;
        }
    }

    void GenerateIV(int key_index, unsigned char* iv)
    {
        for (int i = 0; i < 16; i++)
        {
            iv[i] = (i + 793)^key_index << 1;
        }
    }

    void EncodeHeader(int packet_len, unsigned char* buf)
    {
        int _t = RangeRandom(1, 19);
        packet_len = (packet_len + _t * 100)*32 + _t * 100000000;
        PUTU32(buf, packet_len);
    }

    bool DecodeHeader(int& len, const unsigned char* buf)
    {
        int _l = GETU32(buf);
        len = (_l % 100000000) / 32 - ((_l / 100000000)*100);
        return (len > 8)&&(len <= AES_MAX_BUFFER_LEN);
    }

    int MixKeyIndex(int key_index)
    {
        int _nMixedKeyIndex = key_index + 13455350;
        return _nMixedKeyIndex;
    }

    int DeMixKeyIndex(int key_index)
    {
        int _nDeMixedKeyIndex = key_index - 13455350;
        return _nDeMixedKeyIndex;
    }
public:

    AesWarpper()
    {
        srand(time(NULL));
    }

    ~AesWarpper() { }

    /*
     * [plain_text]: plain text that to be encrypted
     * [encrypted_buf]:encrypted buffer
     * [len]: plain text's length, will be set to 'length of the encrypted buffer'
     */
    void Encrypt(const char* plain_text, unsigned char* encrypted_buf, int& len)
    {
        unsigned char _key[16] = {0};
        unsigned char _iv[16] = {0};
        int _keyIndex = RangeRandom(0, RAND_MAX);
        int _originalLen = len;
        GenerateKey(_keyIndex, _key, 16);
        GenerateIV(_keyIndex, _iv);
    	AES_KEY key;
        AES_set_encrypt_key(_key, 128, &key);

        unsigned char* _pSrcPadded = NULL;
        PadData(plain_text, len, &_pSrcPadded, AES_BLOCK_SIZE);
        len = 4 + 4 + len;
        //4bytes(perplex bytes) + 4bytes(mixed keyindex) + len(_encrypted_bytes)
        EncodeHeader(_originalLen, encrypted_buf);
        PUTU32(encrypted_buf + 4, MixKeyIndex(_keyIndex));
        AES_cbc_encrypt(_pSrcPadded, encrypted_buf + 4 + 4, len - 4 - 4, &key, _iv, AES_ENCRYPT);
        free(_pSrcPadded);
    }

    /*
     * [encrypted_buf]: encrypted buffer that to be decrypted
     * [decrypted_buf]: pointer to decrypted buffer.
     * [len]: encrypted buffer's length, will be set to 'length of the decrypted buffer' if decrypt success.
     * [return]: ture if decrypt success, or false;
     */
    bool Decrypt(const unsigned char* encrypted_buf, unsigned char* decrypted_buf, int& len)
    {
        int _decrypted_len = 0;
        if ((len > MAX_BUFFER_LEN)
                || (DecodeHeader(_decrypted_len, encrypted_buf) == false))
        {
            std::cout<<"AesWarpper::Decrypt(): DecodeHeader() failed. _decrypted_len :" << _decrypted_len << " len:" << len<<std::endl;
            return false;
        }

        unsigned char _key[16] = {0};
        unsigned char _iv[16] = {0};
        int _keyIndex = DeMixKeyIndex(GETU32(encrypted_buf + 4));
        GenerateKey(_keyIndex, _key, 16);
        GenerateIV(_keyIndex, _iv);
	AES_KEY key;
        AES_set_decrypt_key(_key, 128, &key);
        AES_cbc_encrypt(encrypted_buf + 8, decrypted_buf, len - 8, &key, _iv, AES_DECRYPT);
        len = _decrypted_len;
        return true;
    }
};

#endif /* AESWARPPER_H */

