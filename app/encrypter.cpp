#include <fstream>
#include <dirent.h>
#include "encrypter.h"

void scan_dir(const char* dir_path, const char* out_path, void (*fun)(const char*, const char*))
{
	struct dirent** namelist;
	int n =scandir(dir_path, &namelist, 0, 0);
	if(n < 0)
	{
		perror("error in scandir\n");
		return;
	}

	while(n--)
	{
		if(strcmp(".", namelist[n]->d_name) == 0
		||strcmp("..", namelist[n]->d_name) == 0)
		{
			continue;
		}

		char szTemp[512] = {0};	
		char outFile[512] = {0};	
		sprintf(szTemp, "%s/%s", dir_path, namelist[n]->d_name);
		sprintf(outFile, "%s/%s", out_path, namelist[n]->d_name);
		if(namelist[n]->d_type == DT_REG)
		{
			fun(szTemp, outFile);
		}
		
		free(namelist[n]);
	}
	free(namelist);
}

void encrypt_whole_file(const char* file_name, const char* out_file)
{
	printf("encrypt whole file:%s\n", file_name);

	std::ifstream in_fd;
	in_fd.open(file_name);
	in_fd.seekg(0, std::ios::end);
	int len = in_fd.tellg();
	//std::cout<<"size:"<<len<<std::endl;

	char* plain_buff = new char[len];
	in_fd.seekg(0, std::ios::beg);
	in_fd.read(plain_buff, len);
	in_fd.close();
	//std::cout<<plain_buff<<std::endl;
	
	char* encrypted_buff = new char[len*2];
	memset(encrypted_buff, 0, len*2);
	AesWarpper encrypter;
	encrypter.Encrypt(plain_buff, (unsigned char*)encrypted_buff, len);

	std::ofstream out_fd;
	out_fd.open(out_file);
	out_fd.write(encrypted_buff, len);
	out_fd.close();

	delete plain_buff;
	delete encrypted_buff;
}

void decrypt_whole_file(const char* file_name, const char* out_file)
{
	printf("decrypt whole file:%s\n", file_name);

	std::ifstream in_fd;
	in_fd.open(file_name);
        in_fd.seekg(0, std::ios::end);
	int len = in_fd.tellg();

	char* encrypted_buff = new char[len];
        in_fd.seekg(0, std::ios::beg);
        in_fd.read(encrypted_buff, len);
	in_fd.close();

	char* decrypted_buff = new char[len*2];
	memset(decrypted_buff, 0, len*2);
	AesWarpper decrypter;
	decrypter.Decrypt((unsigned char*)encrypted_buff, (unsigned char*)decrypted_buff, len);

	std::ofstream out_fd;
	out_fd.open(out_file);
	out_fd.write(decrypted_buff, len);
	out_fd.close();

	delete decrypted_buff;
	delete encrypted_buff;
}

void encrypt_perK_file(const char* file_name, const char* out_file)
{
	printf("encrypt perK file:%s\n", file_name);
	std::ifstream in_fd;
	in_fd.open(file_name);

	std::ofstream out_fd;
	out_fd.open(out_file);

	char encrypted_text[1024] = {0};
	char decrypted_text[1024] = {0};
	char line[1024] = {0};
	AesWarpper encrypter;

	while(!in_fd.eof())
	{
		memset(line, 0, 1024);
		memset(encrypted_text, 0, 1024);
		memset(decrypted_text, 0, 1024);

		in_fd.getline(line, 512);
		int len = strlen(line);
		if(len <= 0)
		{
			break;
		}

		encrypter.Encrypt(line, (unsigned char*)encrypted_text, len);
		char tmp[4] = {0};
		int* p = (int*)tmp;
		*p = len;
		for(int i=0; i<4; i++)
		{
			out_fd.put(tmp[i]);
		}

		for(int i=0; i<len; i++)
		{
			out_fd.put(encrypted_text[i]);
		}
	}

	out_fd.close();
	in_fd.close();
}

void decrypt_perK_file(const char* file_name, const char* out_file)
{
	printf("decrypt perK file:%s\n", file_name);
	std::ifstream in_fd;
	in_fd.open(file_name);

	std::ofstream out_fd;
	out_fd.open(out_file);

	char decrypted_text[1024] = {0};
	char line[1024] = {0};
	AesWarpper decrypter;

	int index = 0;
	int c;
	while(!in_fd.eof())
	{
		memset(line, 0, 1024);
		memset(decrypted_text, 0, 1024);

		char tmp[4] = {0};
		in_fd.read(tmp, 4);
		int len = *(int*)tmp;

		if(len <= 0)
		{
			break;
		}

		in_fd.read(line, len);

		decrypter.Decrypt((unsigned char*)line, (unsigned char*)decrypted_text, len);
		out_fd.write(decrypted_text, strlen(decrypted_text));
		out_fd.put('\n');
	}

	out_fd.close();
	in_fd.close();
}
